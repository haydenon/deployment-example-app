﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace DeploymentExample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CatController : ControllerBase
    {

        [HttpGet]
        [Route("random")]
        public Cat Get()
        {
            var rng = new Random();
            var index = rng.Next(CatData.Cats.Count());
            return CatData.Cats[index];
        }
    }
}
